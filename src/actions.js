import { store } from './store'
export const GET_USERS = 'GET_USERS'
export const GET_POSTS = 'GET_POSTS'

export const getPosts = () => {
	fetch('https://jsonplaceholder.typicode.com/posts')
		.then(response => response.json())
		.then(json => {
			store.dispatch({
				type: GET_POSTS,
				payload: {
					posts: json
				}
			})
			console.log(store.getState())
		})
}

export const getUsers = () => {
	fetch('https://jsonplaceholder.typicode.com/users')
		.then(response => response.json())
		.then(json => {
			store.dispatch({
				type: GET_USERS,
				payload: {
					users: json
				}
			})
			console.log(store.getState())
		})
}
